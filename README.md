# Setting up a Cluster in EKS
  
Install the [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html)
Install [`jq` the command line JSON parser](https://stedolan.github.io/jq/download/).

Setting up state backend

    terraform init
    terraform apply -target=null_resource.backend_state_setup -var='gitlab-token=not-needed-yet' -var='gitlab-project-id=14446037' -var='cluster-name=gitlab-eks'  

Edit `backend.hcl`

Setting up the cluster

    terraform init -backend-config=backend.hcl
    terraform apply -var='gitlab-token=your-token' -var='gitlab-project-id=your-project-id' -var='cluster-environment=*' -var='cluster-name=gitlab-eks' -var='aws_region=eu-central-1'

In the above command the following variables are optional, and show their default values:

- cluster-name
- cluster-environment
- aws_region

# Setting your cluster size

Adjust `aws_eks_node_group.node.scaling_config` 

# Limitations

- installing Helm and Knative should still be done manually
- once Knative is installed, EKS does not give an IP, but a CNAME record to point to
- the provided IP/CNAME should be set manually

# Resources

This repo is based on https://github.com/terraform-providers/terraform-provider-aws/tree/master/examples/eks-getting-started