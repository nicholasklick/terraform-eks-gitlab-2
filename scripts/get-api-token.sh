#!/bin/bash
# Exit if any of the intermediate steps fail
set -e

# Extract "foo" and "baz" arguments from the input into
# FOO and BAZ shell variables.
# jq will ensure that the values are properly quoted
# and escaped for consumption by the shell.
eval "$(jq -r '@sh "KUBECONFIG=\(.kubeconfig) SECRET=\(.secret)"')"

TOKEN=$(kubectl -n kube-system --kubeconfig $KUBECONFIG describe $SECRET | grep token: | cut -c7-1024 | tr -d '[:space:]')

jq -n --arg token "$TOKEN" '{"token":$token}'